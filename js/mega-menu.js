$(document).ready(function () {

    //Hide relevant elements
    $('#home-button').hide();
    $('#blog-button-menu').hide();

    //Highlight selected div
    $('.menu-button').mouseenter(function () {
        $(this).css('background-color', 'red');
        $(this).css('color', 'white');
        $(".menu-button").not(this).css('background-color', 'white');
        $(".menu-button").not(this).css('color', 'black');
    });

    //Close open menu sections on mouse leave
    $('.mega-menu').mouseleave(function () {
        $('.mega-menu').slideUp();
        $('.menu-button').addClass('unset-bg');
    });

    //Close open menu on page tap (for tablets)
    $('.parallax-section, #textcontainer').click(function () {
        $('.mega-menu').slideUp();
    });

    //Group 1
    $('#group1-button').mouseenter(function () {
        $('.mega-menu:not(#group1-mega-menu)').slideUp();
        $('#group1-mega-menu').slideToggle();
        $('#group1-mega-menu').css('display', 'block');
    });

    //Group 2
    $('#group2-button').mouseenter(function () {
        $('.mega-menu:not(#group2-mega-menu)').slideUp();
        $('#group2-mega-menu').slideToggle();
        $('#group2-mega-menu').css('display', 'block');
    });

    //Group 3
    $('#group3-button').mouseenter(function () {
        $('.mega-menu:not(#group3-mega-menu)').slideUp();
        $('#group3-mega-menu').slideToggle();
        $('#group3-mega-menu').css('display', 'block');
    });

    //Group 4
    $('#group4-button').mouseenter(function () {
        $('.mega-menu:not(#group4-mega-menu)').slideUp();
        $('#group4-mega-menu').slideToggle();
        $('#group4-mega-menu').css('display', 'block');
    });


    //MOBILE

    //Test to see if mobile menu is hidden
    var menuHidden = 0;

    $('#burger-button').click(function () {
        if (menuHidden === 0) {
            //document.body.scrollTop = document.documentElement.scrollTop = 0;
            $('.mega-menu').slideDown();
            $('.parallax-section, #textcontainer').css('display', 'none');
            menuHidden = 1;
        }

        else if (menuHidden === 1) {
            $('.mega-menu').slideUp();
            $('.parallax-section, #textcontainer').css('display', 'block');
            menuHidden = 0;
        }
    });

});
