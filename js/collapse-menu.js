$(window).scroll(function () {

    if (matchMedia) {
        var mq = window.matchMedia("(max-width: 429px)");
        mq.addListener(WidthChange);
        WidthChange(mq);
    }

    function WidthChange(mq) {

        if (mq.matches) {
            /*Enable these by default on mobile view without need for scrolling*/
            $('.top-menu, #pendragon-mobile-logo').addClass('hide');
            $('.menu').addClass('collapse-menu').css({ transition: '1s' });
            $('.menu-button').addClass('collapse-menu-button').css({ transition: '1s' });
            $('.mega-menu').addClass('mega-menu-collapsed');
            $('#home-button, #blog-button-menu').show();

            $('.mega-menu').mouseenter(function () {
                $('.mega-menu').css('display', 'block');
            });

            $('.mega-menu').mouseleave(function () {
                $('.mega-menu').css('display', 'block');
            });
        }
        else {
            /*These are enabled on devices 430px or wider*/
            if ($(this).scrollTop() > 0) {
                $('.top-menu, #pendragon-mobile-logo').addClass('hide');
                $('.menu').addClass('collapse-menu').css({ transition: '1s' });
                $('.menu-button').addClass('collapse-menu-button').css({ transition: '1s' });
                $('.mega-menu').addClass('mega-menu-collapsed');
                $('#home-button, #blog-button-menu').show();
            }

            else {
                $('.top-menu, #pendragon-mobile-logo').removeClass('hide');
                $('.menu').removeClass('collapse-menu').css({ transition: '1s' });
                $('.menu-button').removeClass('collapse-menu-button').css({ transition: '1s' });
                $('.mega-menu').removeClass('mega-menu-collapsed');
                $('#home-button, #blog-button-menu').hide();
            }
        }
    }
});